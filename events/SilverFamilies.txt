namespace = silver_families

country_event = {
	id = silver_families.0
	title = silver_families.0.t
	desc = silver_families.0.d
	picture = DIPLOMACY_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		dynasty = "Dummy"
		is_lesser_in_union = no
	}
			
	option = {
		name = silver_families.0.a
		add_prestige = 10
	}
			
}

country_event = {
	id = silver_families.1
	title = silver_families.1.t
	desc = silver_families.1.d
	picture = DIPLOMACY_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		dynasty = "Silmuna"
		is_lesser_in_union = no
	}
			
	option = {
		name = silver_families.1.a
		add_prestige = 10
	}
			
}

country_event = {
	id = silver_families.2
	title = silver_families.2.t
	desc = silver_families.2.d
	picture = DIPLOMACY_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		dynasty = "Siloriel"
		is_lesser_in_union = no
	}
			
	option = {
		name = silver_families.2.a
		add_prestige = 10
	}
			
}

country_event = {
	id = silver_families.3
	title = silver_families.3.t
	desc = silver_families.3.d
	picture = DIPLOMACY_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		dynasty = "Silcalas"
		is_lesser_in_union = no
	}
			
	option = {
		name = silver_families.3.a
		add_prestige = 10
	}
			
}

country_event = {
	id = silver_families.4
	title = silver_families.4.t
	desc = silver_families.4.d
	picture = DIPLOMACY_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		dynasty = "Silistra"
		is_lesser_in_union = no
	}
			
	option = {
		name = silver_families.4.a
		add_prestige = 10
	}
			
}

country_event = {
	id = silver_families.5
	title = silver_families.5.t
	desc = silver_families.5.d
	picture = DIPLOMACY_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		dynasty = "Silnara"
		is_lesser_in_union = no
	}
			
	option = {
		name = silver_families.5.a
		add_prestige = 10
	}

}

country_event = {
	id = silver_families.6
	title = silver_families.6.t
	desc = silver_families.6.d
	picture = DIPLOMACY_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		dynasty = "Silebor"
		is_lesser_in_union = no
	}
			
	option = {
		name = silver_families.6.a
		add_prestige = 10
	}
			
}

country_event = {
	id = silver_families.7
	title = silver_families.7.t
	desc = silver_families.7.d
	picture = DIPLOMACY_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		dynasty = "Silurion"
		is_lesser_in_union = no
	}
			
	option = {
		name = silver_families.7.a
		add_prestige = 10
	}
			
}

country_event = {
	id = silver_families.8
	title = silver_families.8.t
	desc = silver_families.8.d
	picture = DIPLOMACY_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		dynasty = "Silgarion"
		is_lesser_in_union = no
	}
			
	option = {
		name = silver_families.8.a
		add_prestige = 10
	}
			
}